import re
usernames = ["Samzzz", "Speirs", "Morgan", "tenza", "tytitus", "Ulema"]


def verifyUser(name):
    # test of minimal lenght
    if len(name) >2:
        # test If first letter is uppercase | and only numbers | and string not empty
        if name[0].isupper():
            #test of maximal lenght
            if len(name)<15:
                # test of special character with Regex
                if re.match("^[a-zA-Z0-9_]*$", name):
                    #username already taken
                    if name not in usernames:
                        return True
    
    return False
print(verifyUser(""))
        